import time 
import RPi.GPIO as io 
io.setmode(io.BCM) 

pir_pin = 24 
led_pin = 26

io.setup(pir_pin, io.IN) 
io.setup(led_pin, io.OUT)
io.output(led_pin, False)

while True:
    if io.input(pir_pin):
        print("POWER ON")
        io.output(led_pin, True)
        time.sleep(20);
        print("POWER OFF")
        io.output(led_pin, False)
        time.sleep(5)
    time.sleep(1)
