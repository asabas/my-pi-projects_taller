#Servo 2

import RPi.GPIO as GPIO    #Importamos la libreria RPi.GPIO
import time                #Importamos time para poder usar time.sleep

GPIO.setmode(GPIO.BCM)   #Ponemos la Raspberry en modo BOARD
GPIO.setup(16,GPIO.OUT)    #Ponemos el pin 16 como salida
p = GPIO.PWM(16,50)        #Ponemos el pin 16 en modo PWM y enviamos 50 pulsos por segundo
p.start(7.5)               #Enviamos un pulso del 7.5% para centrar el servo

try:                 
    while True:      #iniciamos un loop infinito

        p.ChangeDutyCycle(7.5)    #Enviamos un pulso del 7.5% para centrar el servo
        time.sleep(1)           #pausa de medio segundo
        p.ChangeDutyCycle(12.5)   #Enviamos un pulso del 12.5% para girar el servo hacia 180
        time.sleep(1)           #pausa de medio segundo
        p.ChangeDutyCycle(2.5)    #Enviamos un pulso del 2.5% para girar el servo 0
        time.sleep(1)           #pausa de medio segundo

except KeyboardInterrupt:         #Si el usuario pulsa CONTROL+C entonces...
    p.stop()                      #Detenemos el servo 
    GPIO.cleanup()                #Limpiamos los pines GPIO de la Raspberry y cerramos el script