#!/usr/bin/env python
#Servo Motor

import RPi.GPIO as GPIO    #Importamos la libreria RPi.GPIO
import time                #Importamos time para poder usar time.sleep

GPIO.setmode(GPIO.BCM)   #Ponemos la Raspberry en modo BOARD
GPIO.setup(16,GPIO.OUT)    #Ponemos el pin 16 como salida


try:                 
    while True:      #iniciamos un loop infinito

        GPIO.output(16, 1)    #Enviamos un pulso alto
        time.sleep(0.0015)      #pausa
        GPIO.output(16, 0)   #Enviamos un pulso cero
        time.sleep(2.0)           #pausa de medio segundo

except KeyboardInterrupt:         #Si el usuario pulsa CONTROL+C entonces..
    GPIO.cleanup()                #Limpiamos los pines GPIO de la Raspberry y cerramos el script